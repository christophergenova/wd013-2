<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>::Birth Stone::</title>
    <link rel="stylesheet" href="https://bootswatch.com/4/minty/bootstrap.css">

</head>
<body>
<div class="d-flex justify-content-center align-items-center vh-100 flex-column">
    <h1>Welcome to verify your Birthstone!</h1>
    <form action="controllers/process_birthstone.php" method="POST" class="form-group">
        <div class="form-group">
            <label for="name">Please Input Name:</label>
            <input type="text" name="user_name" class="form-control">
        </div>
        <div class="form-group">
        <label for="birthday">Birth Month:</label>
            <input type="text" id="birthday" name="birthday" placeholder="Ex: January or 01">
            
        </div>
        <input type="submit" value="Submit" class="btn btn-info">


    </form>

</div>
    
</body>
</html>