<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Age Checker</title>
    <link rel="stylesheet" href="https://bootswatch.com/4/minty/bootstrap.css">
</head>
<body>

    <div class="d-flex justify-content-center align-items-center flex-column vh-100">
    <h1>Age Checker!</h1>

    <!-- METHOD POST -->
    <!-- action = refers to the file that will handle the form -->
    <form action="controllers/process_age_checker.php" method="POST">
        <div class="form-group">
            <label for="age">What's your age?:</label>
            <!-- INPUTS MUST HAVE A NAME -->
            <input type="number" name="age" class="form-control">
            

        </div>
      
        <!-- BUTTON INSIDE THE FORM MUST BE TYPE SUBMIT -->
        <button type="submit" class="btn btn-success">Submit</button>
    </form>

    </div>
</body>
</html>