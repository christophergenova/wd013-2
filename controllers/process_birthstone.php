<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Process_Birthstone</title>
    <link rel="stylesheet" href="https://bootswatch.com/4/minty/bootstrap.css">
</head>
<body>
<div class="d-flex justify-content-center align-items-center vh-100 flex-column bg-info">

    <table class="table-striped">
            <thead>
                <tr>
                    <th>Visitor Name:&nbsp;</th>
                    <th>Birth Stone</th>
                   
                </tr>
            </thead>
            <tbody>

        <?php 

        // $userBirthStone = ["ruby"];


    $userName = $_POST['user_name'];
    $userBirthday = $_POST['birthday'];



 

    // var_dump($userName);
    // var_dump($userBirthday);
    // die();


    if($userName === '' && $userBirthday === ''){
        echo "Please input valid data!";
        return;
    ?>

    <?php
    }else if($userBirthday === 'January' || $userBirthday === '01' ){
    
    ?>
        <!-- echo $userName." " . "Your Birthstone is:"." ". " Garnet"."<br>";
        echo "<img src='https://upload.wikimedia.org/wikipedia/commons/thumb/5/5e/Garnet_Andradite20.jpg/260px-Garnet_Andradite20.jpg'>"; -->
        <tr>
        <td><?php echo $userName ?></td>
        <td><?php echo "Garnet" ?></td>
        <td><?php echo "<img src='https://upload.wikimedia.org/wikipedia/commons/thumb/5/5e/Garnet_Andradite20.jpg/260px-Garnet_Andradite20.jpg'>"?></td>
        </tr>
        <tr>
            <td><p>Your Birth stone for January</p></td>
        </tr>
    <?php    
        
    }else if($userBirthday === 'February' || $userBirthday === '02' ){
    
    ?>
        <tr>
        <td><?php echo $userName ?></td>
        <td><?php echo "Amethyst" ?></td>
        <td><?php echo "<img src='https://upload.wikimedia.org/wikipedia/commons/thumb/e/e2/Amethyst._Magaliesburg%2C_South_Africa.jpg/250px-Amethyst._Magaliesburg%2C_South_Africa.jpg'>"?></td>
        </tr>
        <tr>
            <td><p>Your Birth stone for February</p></td>
        </tr>
    <?php
    }else if($userBirthday === 'March' || $userBirthday === '03' ){
    ?>
        <tr>
            <td><strong><?php echo $userName ?></strong</td>
            <td><?php echo "Bloodstone" ?></td>
            <td><?php echo "<img src='https://upload.wikimedia.org/wikipedia/commons/thumb/8/87/Quarz_-_Heliotrop_%28Blutjaspis%29.JPG/260px-Quarz_-_Heliotrop_%28Blutjaspis%29.JPG'>"?></td>       
        </tr>
        <tr>
            <td><p>Your Birth stone for March</p></td>
        </tr>
    <?php    
    }else if($userBirthday === 'April' || $userBirthday === '04' ){
    ?>
          <tr>
            <td><strong><?php echo $userName ?></strong</td>
            <td><?php echo "Diamond" ?></td>
            <td><?php echo "<img src='https://upload.wikimedia.org/wikipedia/commons/thumb/d/d7/Rough_diamond.jpg/260px-Rough_diamond.jpg'>"?></td>       
        </tr>
        <tr>
            <td><p>Your Birth stone for April</p></td>
        </tr>


    <?php
    }else if($userBirthday === 'May' || $userBirthday === '05' ){
    ?>
            <tr>
            <td><strong><?php echo $userName ?></strong</td>
            <td><?php echo "Emerald" ?></td>
            <td><?php echo "<img src='https://upload.wikimedia.org/wikipedia/commons/thumb/d/df/B%C3%A9ryl_var._%C3%A9meraude_sur_gangue_%28Muzo_Mine_Boyaca_-_Colombie%29_2.jpg/260px-B%C3%A9ryl_var._%C3%A9meraude_sur_gangue_%28Muzo_Mine_Boyaca_-_Colombie%29_2.jpg'>"?></td>       
        </tr>
        <tr>
            <td><p>Your Birth stone for May</p></td>
        </tr>
    
    <?php
    }else if($userBirthday === 'June' || $userBirthday === '06'){
    ?>
         <tr>
            <td><strong><?php echo $userName ?></strong</td>
            <td><?php echo "Pearl" ?></td>
            <td><?php echo "<img src='https://upload.wikimedia.org/wikipedia/commons/thumb/5/5e/Various_pearls.jpg/210px-Various_pearls.jpg'>"?></td>       
        </tr>
        <tr>
            <td><p>Your Birth stone for June</p></td>
        </tr>

    <?php
    }else if($userBirthday === 'July' || $userBirthday === '07'){
    ?>
         <tr>
            <td><strong><?php echo $userName ?></strong</td>
            <td><?php echo "Ruby" ?></td>
            <td><?php echo "<img src='https://upload.wikimedia.org/wikipedia/commons/thumb/4/49/Corundum-winza-17d.jpg/260px-Corundum-winza-17d.jpg'>"?></td>       
        </tr>
        <tr>
            <td><p>Your Birth stone for July</p></td>
        </tr>

    <?php
    }else if($userBirthday === 'August' || $userBirthday === '08'){

    ?>
          <tr>
            <td><strong><?php echo $userName ?></strong</td>
            <td><?php echo "Sardonyx" ?></td>
            <td><?php echo "<img src='https://upload.wikimedia.org/wikipedia/commons/thumb/1/1e/Red_onyx_-_Handicraft.jpg/220px-Red_onyx_-_Handicraft.jpg'>"?></td>       
        </tr>
        <tr>
            <td><p>Your Birth stone for August</p></td>
        </tr>

    
    <?php
    }else if($userBirthday === 'September' || $userBirthday === '09'){
    ?>
         <tr>
            <td><strong><?php echo $userName ?></strong</td>
            <td><?php echo "Sapphire" ?></td>
            <td><?php echo "<img src='https://upload.wikimedia.org/wikipedia/commons/thumb/b/be/Logan_Sapphire_SI.jpg/220px-Logan_Sapphire_SI.jpg'>"?></td>       
        </tr>
        <tr>
            <td><p>Your Birth stone for September</p></td>
        </tr>


    <?php
    }else if($userBirthday === 'October' || $userBirthday === '10'){
    ?>
            <tr>
            <td><strong><?php echo $userName ?></strong</td>
            <td><?php echo "Opal" ?></td>
            <td><?php echo "<img src='https://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/Opal-53714.jpg/220px-Opal-53714.jpg'>"?></td>       
        </tr>
        <tr>
            <td><p>Your Birth stone for October</p></td>
        </tr>
        

    <?php
    }else if($userBirthday === 'November' || $userBirthday === '11'){
    ?>
        <tr>
            <td><strong><?php echo $userName ?></strong</td>
            <td><?php echo "Topaz" ?></td>
            <td><?php echo "<img src='https://upload.wikimedia.org/wikipedia/commons/thumb/2/24/Topaze%2C_quartz_fum%C3%A9_1.jpg/270px-Topaze%2C_quartz_fum%C3%A9_1.jpg'>"?></td>       
        </tr>
        <tr>
            <td><p>Your Birth stone for November</p></td>
        </tr>

    
    <?php
    }else if($userBirthday === 'December' || $userBirthday === '12'){ 
    ?>
        <tr>
            <td><strong><?php echo $userName ?></strong</td>
            <td><?php echo "Turquoise" ?></td>
            <<td><?php echo "<img src='https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Turquoise-40031.jpg/220px-Turquoise-40031.jpg'>"?></td>     
        </tr>
        <tr>
            <td><p>Your Birth stone for December</p></td>
        </tr>

    <?php
    }else {
    ?>
        <tr>
            <td><strong><?php echo "Invalid Data!" ?></strong></td>
        </tr>

    <?Php   
    }
    ?>

</div>
</body>
</html>
